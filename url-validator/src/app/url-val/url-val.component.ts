import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-url-val',
  templateUrl: './url-val.component.html',
  styleUrls: ['./url-val.component.css']
})
export class UrlValComponent implements OnInit {

  form: FormGroup = this.formBuilder.group({
    url: new FormControl('', [
      Validators.required,
      this.urlValidator(),
    ])
  });

  constructor(private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
  }

  urlValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const value = control.value;
      var pattern = new RegExp(
        '^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

      if (pattern.test(value)) {
        return null;
      } else {
        return { isIdentifierError: true };
      }
    };
  }

}
