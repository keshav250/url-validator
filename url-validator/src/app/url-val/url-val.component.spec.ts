import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UrlValComponent } from './url-val.component';

describe('UrlValComponent', () => {
  let component: UrlValComponent;
  let fixture: ComponentFixture<UrlValComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UrlValComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UrlValComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
